# Ubuntu Server
## LAMP
```
sudo apt-get update
sudo apt-get install apache2 mysql-server php php-mysql libapache2-mod-php php-xml php-mbstring
sudo apt-get install php-intl imagemagick inkscape php-gd php-cli curl unzip zip git
rm -R /var/www/html
cd /var
sudo service apache2 restart
```
## FTP Server
### vsftpd
```
sudo adduser thorsten
sudo adduser thorsten --home /var/www/html
chmod -R 775 /var/www/html/
sudo apt-get install vsftpd 
sudo nano /etc/vsftpd.conf
------Datei ändern ---- 
local_enable=YES
write_enable=YES
nopriv_user=www-data
------Datei ende----
sudo service vsftpd restart 
```
## Nodes.js
```
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install -y build-essential
```
## Tomcat8
```
sudo apt-get update
apt-get install tomcat8
```
## ASP.4
```
sudo apt-get update
sudo apt-get install mono-xsp4-base
sudo apt-get install apache2 mono-xsp4 mono-complete mono-apache-server4 libapache2-mod-mono mono-reference-assemblies-4.0
sudo a2enmod mod_mono_auto
sudo service apache2 restart


Mediwiki
--------

Willkommen auf meine Seite, Wie heißen Sie.

Hallo
-----
```

