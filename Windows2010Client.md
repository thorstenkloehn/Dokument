
# Windows Software 
## SSH Programme
* Putty https://www.chiark.greenend.org.uk/~sgtatham/putty/
## FTP Programme
* Filezilla https://filezilla-project.org/
## Git Programme
* Git https://git-scm.com/
## Java Umgebung
* Java 10  http://www.oracle.com/technetwork/java/javase/downloads/index.html
* Java 8   http://www.oracle.com/technetwork/java/javase/downloads/index.html
## IDE Programme
* Visual Studio https://www.visualstudio.com/de/
* Visual Studio Code https://www.visualstudio.com/de/
* Eclipse https://www.eclipse.org/
* Netbeans https://netbeans.org/
## AndeHand Programme
* HandBrake https://handbrake.fr/
* Universal USB Installer  http://www.pendrivelinux.com/universal-usb-installer-easy-as-1-2-3/

[test](http://www.webprogrammieren.de)

