## Backup Server

    mysqldump -u root -p --all-databases > sicherung.sql 
    cd /var
    zip -r www.zip www
    chmod 777 -R www

## Zurücksichern

    sudo apt-get update
    sudo apt-get install apache2 mysql-server php php-mysql libapache2-mod-php php-xml php-mbstring
    sudo apt-get install php-intl imagemagick inkscape php-gd php-cli curl unzip zip git
    rm -R /var/www/html
    cd /var
    sudo service apache2 restart
    mysql -u root -p < sicherung.sql 
    unzip www.zip

## Gitea

    cd /var/www/Gitea
    chmod +x gitea 
    ./gitea web &
